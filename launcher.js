//@ts-check
import { fileURLToPath } from 'url';
import concurrently from 'concurrently';

let PORT = 8234;
let HOST = '0.0.0.0';

export let commands = [
  /*
  {
    name: 'react',
    command: 'npm-run react-scripts start',
    env:    {PORT: PORT+1, HOST:HOST, BROWSER:'none'},
    prefixColor: 'blue',
  },
  */
  {
    name: 'api',
    command: 'cd api && npm-run nodemon',
    env:    {PORT: PORT+2, HOST:HOST},
    prefixColor: 'lime',
  },
  /*
  {
    name: 'api-py',
    command: 'cd api-py && npm-run nodemon',
    env:    {PORT: PORT+3, HOST:HOST, FLASK_APP:'server.py',  FLASK_ENV:'development'},
    prefixColor: 'yellow',
  },
  */
  {
    name: 'proxy',
    prefixColor: 'magenta',
    command: 'node reverse-proxy.js',
    env:    {
      PORT: PORT,
      HOST: HOST,
      LAUNCH: false,
      LAUNCH_DELAY: 2000,
      LAUNCH_URL: `${HOST}:${PORT}`,
      RULES_JSON: JSON.stringify({
        rules:{
          '/favicon.ico': `http://${HOST}:${PORT+2}/files/favicon.ico`,
          '/files/admin': `http://${HOST}:${PORT+1}`,
          '/files': `http://${HOST}:${PORT+2}/files`,
       },
       default: `http://${HOST}:${PORT+2}`,
      }),
    },
  },
];


export const concurrent = async (commands)=> {
  return await concurrently(commands, {
    prefix: 'name',
    killOthers: ['failure', 'success'],
  });
};

if (process.argv[1] === fileURLToPath(import.meta.url)) {
  concurrent(commands).then(()=>{
    console.log('EXITING CONCURRENT LAUNCHER');
  });
}
