//@ts-check
import express from 'express';
import crypto from 'crypto';
import directory from 'serve-index';
import { readFileSync, readdirSync, lstatSync, existsSync } from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import kill from 'tree-kill';
import cookieParser from 'cookie-parser';
import basicAuth from 'express-basic-auth';
//console.log(crypto.getHashes());

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const P_DIR = path.join(__dirname, '..');
const SERVER_KEY = ('' + readFileSync(path.join(P_DIR, 'secret.key'))).trim();
const authenticationHandler = basicAuth({
  users: JSON.parse('' + readFileSync(path.join(P_DIR, 'passwords.json'))),
  challenge: true,
  unauthorizedResponse: (req) => (req.auth ?
    ('Credentials ' + req.auth.user + ':' + req.auth.password + ' rejected')
    : 'No credentials provided'),
})

/** @param {string} b64 */
function myBase64(b64) {
  return b64.replace(/\+/g, '_').replace(/\//g, '-').replace(/=/g, '');
}

/** @param {string} key @param {string} text */
function hmacSign(key, text) {
  const hmac = crypto.createHmac('md5', key);
  hmac.update(text);
  return myBase64(hmac.digest('base64'));
}
console.log(hmacSign(SERVER_KEY, '/files'));

/** @param {string} rel @param {string} key */
function authenticate(rel, key) {
  if (key == '') return false;
  let p = rel;
  while (p != '/') {
    const trueKey = hmacSign(SERVER_KEY, p);
    if (key == trueKey) return true;
    if (p == '/files') break;
    p = path.dirname(p);
  }
  return false;
}

/** @param {string} rel @param {boolean} isIndex */
function publicAccess(rel, isIndex) {
  if (isIndex) return false;
  if ((rel + '/').startsWith('/files/private/')) return false;
  return true;
}

/** @param {string} dir */
function htmlDirIndex(dir) {
  // Recursive list of links with access keys
  // maxDepth prevents symlinks loops
  /** @param {string} rel @param {number} maxDepth */
  const getTree = (rel, maxDepth) => {
    const p = path.join(P_DIR, rel);
    const e = {
      rel: rel,
      key: hmacSign(SERVER_KEY, rel),
      isDir: lstatSync(p).isDirectory(),
    };
    let paths = [e];
    if (maxDepth > 0 && e.isDir && !rel.startsWith('.')) {
      for (const f of readdirSync(p)) {
        paths = [...paths, ...getTree(path.join(rel, f), maxDepth - 1)];
      }
    }
    return paths;
  }
  const files = getTree(dir, 15);
  const elems = files.filter(e => '/files/.debris' != e.rel.slice(0, '/files/.debris'.length)).map(e =>
    `<li><a href="${e.rel}?${e.isDir ? 'index&' : ''}key=${e.key}">${e.rel}</a></li>`);
  return `<html><body><ol>${elems.join('')}</ol></body></html>`;
}

/** @param {express.Request} req  @param {express.Response} res  @param {express.NextFunction} next */
function checkAuthorization(req, res, next) {
  const rel = req.baseUrl + req.path;
  const lastKey = '' + ((req.signedCookies && req.signedCookies['lastKey']) || '');
  let key = '' + (req.query.key || lastKey);
  if (key.length && key != lastKey) res.cookie('lastKey', key, { signed: true, maxAge: 1000 * 3600 });
  const isIndex = req.query.index !== undefined;
  const auth = publicAccess(rel, isIndex) || authenticate(rel, key);
  if (auth) return next();
  return authenticationHandler(req, res, next);
}

/** @param {express.Request} req  @param {express.Response} res  @param {express.NextFunction} next */
function serveFiles(req, res, next) {
  const rel = req.baseUrl + req.path;
  const lastKey = '' + ((req.signedCookies && req.signedCookies['lastKey']) || '');
  let key = '' + (req.query.key || lastKey);
  if (key.length && key != lastKey) res.cookie('lastKey', key, { signed: true, maxAge: 1000 * 3600 });
  const isIndex = req.query.index !== undefined;
  if (isIndex) return res.send(htmlDirIndex(rel));
  const pRel = path.join(P_DIR, rel);
  if (!existsSync(pRel) || lstatSync(pRel).isDirectory()) return next();
  return res.sendFile(path.join(P_DIR, rel));
}

/** @param {number} port @param {string} host */
export function serve(port, host = 'localhost') {
  const app = /**@type {express.Application}*/ express();
  app.use('/files/', cookieParser('SomeSecret...1238738h4dhd-9huh08sd7cwe4rnl5xo'));
  app.use('/files/', checkAuthorization);
  app.use('/files/', serveFiles);
  app.use('/files/', directory(path.join(P_DIR, '/files/')));
  app.listen(port, host, function () {
    console.log(`Public server running at http://${host}:${port}`);
  });
}

if (process.argv[1] === fileURLToPath(import.meta.url)) {
  const pid = process.pid;
  process.on('SIGINT', () => kill(pid, 'SIGKILL'));
  /** @type {number} */
  const port = parseInt(process.env.PORT || '3458');
  /** @type {string} */
  const host = process.env.HOST || 'localhost';
  serve(port, host);
}