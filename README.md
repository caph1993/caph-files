# Caph Files (private repo)

Serve files:

* Serves files in `./files/`
* Serves files in `./files/private/` with secret URLs only
* There is no configuration database, just a secret key

# Requisites (node, npm and the following libraries):

`npm install typescript ts-node open concurrently nodemon http-proxy http-proxy-rules tree-kill`
`npm install express @types/express`
`npm install serve-index @types/serve-index`
