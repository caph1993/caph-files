//@ts-check
import httpProxy from 'http-proxy';
import HttpProxyRules from 'http-proxy-rules';
import { fileURLToPath } from 'url';
import http from 'http';


export const proxy_launch = async (host, port, proxyRules)=> {
  var proxy = httpProxy.createProxy();
  proxy.on('error', (err)=>{
    console.error(err);
  });
  const server = http.createServer(function(req, res) {
    const target = proxyRules.match(req);
    if(target){
      proxy.web(req, res, {target: target});
    } else{
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end('The request url and path did not match any of the listed rules!');
    }
  });
  server.listen(port, host);
  server.on('upgrade', function (req, socket, head) {
    const target = proxyRules.match(req);
    proxy.ws(req, socket, head, {target:target, timeout:0});
  });
  return server;
};

const launch_browser = async (url, start_delay)=>{
  if(start_delay) await new Promise((ok,err)=>setTimeout(ok, start_delay));
  const open = require('open');
  open(url);
};


if (process.argv[1] === fileURLToPath(import.meta.url)) {
  const rules = JSON.parse(process.env.RULES_JSON||'null')||{
    // example:
    rules: {
      '.*/test': 'http://localhost:8080/cool', // Rule (1)
      '.*/test2/': 'http://localhost:8080/cool2/', // Rule (2)
      '/posts/([0-9]+)/comments/([0-9]+)': 'http://localhost:8080/p/$1/c/$2', // Rule (3)
      '/author/([0-9]+)/posts/([0-9]+)/': 'http://localhost:8080/a/$1/p/$2/' // Rule (4)
    },
    default: 'http://localhost:8080' // default target
  };
  var proxyRules = new HttpProxyRules(rules);
  const port = process.env.PORT||3000;
  const host = process.env.HOST||'localhost';
  console.log(`Starting proxy at ${host}:${port} with rules:`, rules);
  const proxy = proxy_launch(host, port, proxyRules);
  if(JSON.parse(process.env.LAUNCH||'false')){
    const launch_delay = process.env.LAUNCH_DELAY||0;
    const launch_url = process.env.LAUNCH_URL||`${host}:${port}`;
    launch_browser(launch_url, launch_delay)
  }
}

